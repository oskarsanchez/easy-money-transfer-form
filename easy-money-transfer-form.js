class EasyMoneyTransferForm extends Polymer.Element {

  static get is() {
    return 'easy-money-transfer-form';
  }

  static get properties() {
    return {
      id: {
        type: Number
      },
      type: {
        type: String
      },
      amount: {
        type: Number
      },
      currency: {
        type: String,
        value: "EUR"
      },
      date: {
        type: Date
      },
      subject: {
        type: String
      },
      account: {
        type: Object
      },
      _account: {
        type: Object,
        computed: '_computeAccount(account)',
        notify: true
      },
      accounts: {
        type: Array
      },
      accountTargetId: {
        type: Number
      },
      ibanTarget: {
        type: String
      },
      loading:{
        type: Boolean,
        notify: true,
        value: false
      },
      isTransfer: {
        type: Boolean
      }
    };
  }

  reset(){
    this.amount = null;
    this.subject = null;
    this.$.radioGroupTipoTransferencia.selected = "0";
    this.$.radioGroupCuentas.selected = "";
    this.ibanTarget = null;
    this.beneficiary = null
  }
  _computeAccount(account){
    if (account == null)
      return null;
    let accountItem = {};
    accountItem.name = account.type;
    accountItem.description = {'value': account.iban, 'masked': false};
    accountItem.primaryAmount = {'label': 'Disponible', 'amount': account.balance, 'currency': 'EUR'};
    accountItem.id = account.id;
    accountItem.imgSrc = cells.uriEndpointWeb + "/resources/images/imgCuentaPersonal.png";
    return accountItem;
  }

  _handleCancelarClick(evt){
    this.dispatchEvent(new CustomEvent('transfer-form-cancel',{bubbles:true, composed:true}));
  }
  _handleAceptarClick(evt){
    this.loading = true;
    const normalizedTransfer = {
      type: this.type,
      amount: parseInt(this.amount),
      currency: this.currency,
      date: this.date,
      subject: this.subject,
      beneficiary: this.beneficiary,
      iban_target: this.ibanTarget,
      account_origin_id: this.account.id
    };
    if (!this.isTransfer)
      normalizedTransfer.account_target_id = parseInt(this.accountTargetId);
    this.$.addtransferdp.headers = {
      authorization : "JWT " + sessionStorage.getItem("token"),
      user_id : parseInt(sessionStorage.getItem("userId"))
    };
    this.$.addtransferdp.body = normalizedTransfer;
    this.$.addtransferdp.host = cells.urlEasyMoneyBankBankServices;
    this.$.addtransferdp.generateRequest();
  }

  _handleRequestAddTransferSuccess(evt) {
    const detail = evt.detail;
    this.loading = false;
    let msg;
    let accountBalance;
    if (detail) {
      delete detail.headers;
      msg = detail.msg;
      accountBalance = detail.accountBalance;
    }
    this.dispatchEvent(new CustomEvent('add-transfer-ok', {
      bubbles:true,
      composed:true,
      detail: {
            msg: msg,
            accountBalance: accountBalance
      }
    }));
  }

  _handleRequestAddTransferError(evt) {
    const detail = evt.detail;
    let msg;
    if (detail) {
      delete detail.headers;
      msg = detail.msg
    }
    this.loading = false;
    this.dispatchEvent(new CustomEvent('add-transfer-error', {
      bubbles: true,
      composed: true,
      detail: {
        msg: msg
      }
    }));
  }

  _handleTipoTransferenciaGroupSelectedChange(evt){
    const detail = evt.detail;
    delete detail.headers;
    this.type = (detail.value == 0)? "Traspaso" : "Transferencia";
    this.isTransfer = (detail.value == 0)? false : true;
  }

  _handleCuentasGroupSelectedChange(evt){
    const detail = evt.detail;
    if (detail){
      delete detail.headers;
      if (this.$.radioGroupCuentas.radios[detail.value]){
        this.accountTargetId = this.$.radioGroupCuentas.radios[detail.value].id;
        this.ibanTarget = this.$.radioGroupCuentas.radios[detail.value].textContent;
      }
    }
  }

  _handleDateChanged(evt){
    const detail = evt.detail;
    if (detail) {
      delete detail.headers;
      this.date = detail.split('-').reverse().join('/');
    }
  }
}
customElements.define(EasyMoneyTransferForm.is, EasyMoneyTransferForm);
